(function(app) {
  app.AppModule =
    ng.core.NgModule({
      imports: [ 
      	ng.platformBrowser.BrowserModule
      ],
      declarations: [ 
      	app.AppComponent,
      	app.CMJMenuComponent,
      	app.CMJMenuButtonComponent,
      	app.CMJMenuSearchComponent
      ],
      bootstrap: [ 
      	app.AppComponent
      ]
    })
    .Class({
      constructor: function() {}
    });
})(window.app || (window.app = {}));