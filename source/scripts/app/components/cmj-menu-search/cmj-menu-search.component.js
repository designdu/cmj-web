(function(app) {
  app.CMJMenuSearchComponent =
    ng.core.Component({
      selector: 'cmj-menu-search',
      templateUrl: 'public/templates/cmj-menu-search/cmj-menu-search.component.html'
    })
    .Class({
      constructor: function() {},
	  openSearch: function() {
	    document.querySelector('.cmj-menu-search').className = 'cmj-menu-search open';
	  },
	  closeSearch: function() {
	    document.querySelector('.cmj-menu-search').className = 'cmj-menu-search';
	  }
    });
})(window.app || (window.app = {}));