(function(app) {
  app.CMJMenuComponent =
    ng.core.Component({
    	selector: 'cmj-menu',
		templateUrl: 'public/templates/cmj-menu/cmj-menu.component.html'
    })
    .Class({
		constructor: function() {
			this.closed             = true;
			this.showMoreText       = 'More';
			this.showMoreButtonIcon = 'down';
		},
		openMenu: function() {
			document.querySelector('.cmj-menu-navigation').className = 'cmj-menu-navigation open';
		},
		closeMenu: function() {
			document.querySelector('.cmj-menu-navigation').className = 'cmj-menu-navigation';
		},
		toggleTabletMenu: function(el){
			if(this.closed == true)
			{
				this.showMore();
				this.closed 		    = false;
				this.showMoreText       = 'Less';
				this.showMoreButtonIcon = 'up';	
			}
			else
			{
				this.showLess();
				this.closed 		    = true;
				this.showMoreText       = 'More';
				this.showMoreButtonIcon = 'down';
			}
		},
		showMore: function(){
			document.querySelector('#show-more-menu').className = 'cmj-menu-navigation-items';  
		},
		showLess: function(){
			document.querySelector('#show-more-menu').className += ' hidden-menu-items';
		}
    });
})(window.app || (window.app = {}));