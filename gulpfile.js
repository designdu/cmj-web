var gulp         = require('gulp');
var sass         = require('gulp-sass');
var uglify       = require('gulp-uglifyjs');
var rename       = require('gulp-rename');
var streamqueue  = require('streamqueue');
var concat       = require('gulp-concat');

gulp.task('default', ['icons', 'compile', 'scss', 'templates', 'watch']);

gulp.task('scss', function() {
	return gulp
		.src('source/styles/cmj-styles.scss')
		.pipe(sass({ errLogToConsole: false, outputStyle: 'compressed' }))
        .pipe(rename('cmj-styles.css'))
		.pipe(gulp.dest('public'))
});

gulp.task('compile', function() {		
	return streamqueue({ objectMode: true },
		/* It is very important that components and modules are loaded in the correct order for angular to pick them up */
	    gulp.src('source/scripts/app/components/**/*.js'),
	    gulp.src('source/scripts/**/*.js')
	)
    .pipe(concat('cmj-app.js'))
    .pipe(gulp.dest('public'))
});

gulp.task('uglify', ['compile'], function() {	
	return gulp.src('public/cmj-app.js')
    .pipe(uglify('cmj-app.js'))
    .pipe(gulp.dest('public'))
});

gulp.task('templates', function() {
	gulp.src('source/scripts/app/templates/**/**.*')
        .pipe(gulp.dest('public/templates'));
});

gulp.task('icons', function() {
	gulp.src('source/icons/icomoon.*')
        .pipe(gulp.dest('public/icons'));
});

gulp.task('watch', function () {
	gulp.watch('source/styles/**/*.scss', ['scss']);
	gulp.watch('source/scripts/**/*.js', ['compile']);
	gulp.watch('source/scripts/app/templates/**/**.*', ['templates']);
});
